Environment: Git Bash , Python 2.7 interpreter and IDLE


1. Run the mbox_json_splits.py against an mbox file 
	This is a python 2.7 version script. enronmbox is the mbox file

	example: python mailboxes__jsonify_mbox.py /c/dev/aid4mail_Prof/enronmbox

	The output will be json files split by 99 MB size. This is to faciliate transfer to AWS ElasticSearch which has a http payload threshold of 100 MB for large instances

The python script is derived from https://github.com/ptwobrussell/Mining-the-Social-Web/blob/master/python_code/mailboxes__jsonify_mbox.py. The script has been enhanced to include json file splits and to add json formatted object as required by AWS


2. Run the bash script to create the AWS ElasticSearch index and mapping types. Upload the Json files to AWS ES one at a time using bulk api and redirect the curl output to stdout.txt for logging.

	** curl is used to send XPOST and XGET requests to an AWS ES endpoint

	Note: bvox__index_creation_mapping is a text file with individual curl commands for index creation, mapping and data upload if you want to run it on a prompt. These commands can be used instead of using option 2 above.

	The mbox_json_files folder has a sample json file (1 of 15) for testing with the script for upload. The text_mboxrd is a test mbox for a sample user from the enron emails