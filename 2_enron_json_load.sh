#!/bin/sh
#
# Create index, mapping and load enron data into elasticsearch

echo "Indexing enron emails"
es_host="http://search-bvox-b4kkkrtmxykyqvo7rxfpr7bf5a.us-east-2.es.amazonaws.com"
curl -XDELETE '$es_host/bvox'

curl -XPUT '$es_host/bvox' -d '{
"settings":
{
    "number_of_shards": 1
}
}'

curl -XPUT '$es_host/bvox/email/_mapping' -d '{
        "email": {
            "properties": {
                "Bcc": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Cc": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Content-Transfer-Encoding": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Content-Type": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Date": {
                    "type" : "date",
                    "format" : "EEE, dd MMM YYYY HH:mm:ss Z"
                },
                "From": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Message-ID": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Mime-Version": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "Subject": {
                    "type": "string"
                },
                "To": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-FileName": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-Folder": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-From": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-Origin": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-To": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-bcc": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "X-cc": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "bytes": {
                    "type": "long"
                },
                "offset": {
                    "type": "long"
                },
                "parts": {
                    "dynamic": "true",
                    "properties": {
                        "content": {
                            "type": "string"
                        },
                        "contentType": {
                            "type": "string",
                            "index": "not_analyzed"
                        }
                    }
                }
            }
        }
}'

X=0
while [ $X -le 16 ]
do
	curl -XPOST '$es_host/bvox/_bulk' --data-binary @enronmboxf-$X.json > stdout.txt
	echo "Finished uploading enronmboxf-$X.json"
	X=$((X+1))
done